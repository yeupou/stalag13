<?php 
$OC_Version = array(17,0,5,0);
$OC_VersionString = '17.0.5';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '16.0' => true,
    '17.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2020-03-11T14:32:51+00:00 db077774e6f893610419a96efc385e5d2cdf111d';
$vendor = 'nextcloud';
