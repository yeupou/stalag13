#!/usr/bin/perl
#
# Copyright (c) 2003-2017 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA
#
# ufw and all are fine to set up firewall rules from scratch.
# A bit a pain in the ass if you want rules to be generated on the fly
# added to prexisting rules, for instance by another software like lxc-net
# 
# this script purpose is only that: setting up extra rules, not managing
# current one, not saving current one, etc.
#
# assumes:
#   - you have two interface, one to the internet and one local

use strict;
use Fcntl ":flock";
use Getopt::Long;
use Sys::Syslog; 
use IO::Interface::Simple;

#######################################
### get options, variables setup

# variables
my ($getopt, $help, $booting);
my ($debug, $verbose);
my $iptables = "/sbin/iptables";

my $rc = "/etc/default/firewall-supplement";
my $internet_interface = "eth0";
my $intranetA_interface = "eth1";
my $intranetB_interface = "";
my $loopback_interface = "lo";
my $on_boot = 0;
my $create_natA = 0;
my $create_natB = 0;

# IP will permanent access through internet 
my @internet_priviledged_ips = ();

# ports opened to the internet interface
#   syntax: port (open), port->IP:port (open and forward)
my @internet_ports = (
    "22", # ssh
    "2222->172.16.0.22:22"); # ssh to local ip through port 2222

# ports opened to the intranet interface
#   syntax: port (open), port->IP:port (open and forward)
my @intranetA_ports = (
    "*", # wildcard accept all
    "22->172.16.0.22"); # ssh to local ip

my @intranetB_ports = @intranetA_ports;


# get config
open(RCFILE, "< $rc");
while(<RCFILE>){
    # ignore comments
    next if /^#/;
    next if /^;/;
    
    # remove quotes
    s/\"//;
    # remove white spaces
    s/\s//;

    # general options
    $on_boot = $1 if /^on_boot=(\d*)$/i;
    $create_natA = $1 if /^create_nat1?=(\d*)$/i;
    $create_natB = $1 if /^create_nat2=(\d*)$/i;

    # internet 
    $internet_interface = $1 if /^internet_interface=(.*)$/i;
    @internet_ports = split(",", $1) if /^internet_ports=(.*)$/i;
    @internet_priviledged_ips = split(",", $1) if /^internet_priviledged_ip=(.*)$/i;
	
    # intranet
    $intranetA_interface = $1 if /^intranet_interface1?=(.*)$/i;
    @intranetA_ports = split(",", $1) if /^intranet_ports1?=(.*)$/i;
    
    # secondary intranet
    $intranetB_interface = $1 if /^intranet_interface2=(.*)$/i;
    @intranetB_ports = split(",", $1) if /^intranet_ports2=(.*)$/i;

    # loopback 
    $loopback_interface = $1 if /^loopback_interface=(.*)$/i;
    
}
close(RCFILE);

# get command line args
eval {
    $getopt = GetOptions("help" => \$help,
			 "debug" => \$debug,
			 "verbose" => \$verbose,
			 "nat" => \$create_natA,
			 "booting" => \$booting);
};

# silently exits if start by boot script will not configure to do so
exit if $booting and ! $on_boot;

if ($help) {
    print STDERR "
  Usage: $0 [OPTIONS]

      Add decent iptables rules to a gateway/router. Nothing overly complicated,
      it mainly filters internet/intranet input and allow to redirect it.
 
    -n,  --nat            Create NAT from $internet_interface to $intranetA_interface.
    -v,  --verbose        Non silent.
         --debug          Only print what it would do (imply --verbose).
	 
  Config ($rc):	 
  ON_BOOT=$on_boot

    internet:
  INTERNET_INTERFACE=$internet_interface
  INTERNET_PORTS=".join(",", @internet_ports)."
  INTERNET_PRIVILEDGED_IP=".join(",", @internet_priviledged_ips)."

    intranet:
  INTRANET_INTERFACE=$intranetA_interface
  INTRANET_PORTS=".join(",", @intranetA_ports)."
  CREATE_NAT=$create_natA

  INTRANET_INTERFACE2=$intranetB_interface
  INTRANET_PORTS2=".join(",", @intranetB_ports)."
  CREATE_NAT2=$create_natB

    loopback:
  LOOPBACK_INTERFACE=$loopback_interface

Author: yeupou\@gnu.org
       http://yeupou.wordpress.com/
";
exit(1);
}

#######################################
### function

sub filter {
    # verbose
    print "$iptables ".join(' ', @_)."\n" if $verbose or $debug;
    # execute unless debug mode
    system($iptables, @_) unless $debug;    
}

sub comment {
    # verbose
    print "# ".join(' ', @_)."\n" if $verbose or $debug;
}

sub filter_list {    
    # interface is always first argument
    my ($interface, @request) = @_;
    # go thru the list
    for (@request) {
	# might be something like PORT->NEWIP:NEWPORT
	my ($port_in, $to) = split("->", $_);
	my ($to_ip, $to_port) = split(":", $to);

	if ($port_in eq '*') {
	    # wilcard *: open all on interface
	    comment("\twildcard: interface fully opened");
	    filter("--append", "INPUT",
		   "--in-interface", $interface,
		   "--jump", "ACCEPT");
	    filter("--append", "OUTPUT",
		   "--out-interface", $interface,
		   "--jump", "ACCEPT");
	    next;
	}
	if ($to_port) {
	    # PORT->IP:PORT
	    comment("\tTCP port $port_in forwarded to $to_ip port $to_port");
	    filter("--table", "nat",
		   "--append", "PREROUTING",
		   "--protocol", "tcp",
		   "--in-interface", $interface,
		   "--dport", $port_in,
		   "--jump", "DNAT",
		   "--to-destination", $to_ip.":".$to_port);
	    next;
	}	
	if ($to_ip) {
	    # PORT->IP
	    comment("\tTCP port $port_in forwarded to $to_ip");
	    filter("--table", "nat",
		   "--append", "PREROUTING",
		   "--protocol", "tcp",
		   "--in-interface", $interface,
		   "--dport", $port_in,
		   "--jump", "DNAT",
		   "--to-destination", $to_ip.":".$port_in);	    
	    next;
	}
	if ($port_in) {
	    # simple port, fully open this specific port
	    # (both TCP and UDP: nowhere else we work on UDP in this script,
	    # seems uncessary so far)
	    comment("\tTCP/UDP port $port_in opened");
	    foreach my $protocol ("tcp", "udp") {
		filter("--append", "INPUT",
		       "--in-interface", $interface,
		       "--protocol", $protocol,
		       "--dport", $port_in,
		       "--jump", "ACCEPT");
	    }
	    next;
	}
	# we should not reach this point, something amiss
       	filter("unable to understand parameters: $_");
    }
}


#######################################
### run

# disallow concurrent run
open(LOCK, "< $0") or die "Failed to ask lock. Exiting";
flock(LOCK, LOCK_EX | LOCK_NB) or die "Unable to lock. This daemon is already alive. Exiting";

# check if the network interface differs (otherwise it would mean
# setup is broken)
die "Both internet ($internet_interface) and intranet ($intranetA_interface) interface have the same name. How come? Please check:\n\t$0 --help\nExit" if $internet_interface eq $intranetA_interface;
# check if each configured network interface actually exists
foreach my $interface ($internet_interface, $intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    die "Interface $interface not found. Please check:\n\t$0 --help\nExit" unless IO::Interface::Simple->new($interface) eq $interface;
}

# log startup
openlog("firewall-supplement", "pid", "LOG_DAEMON");
syslog("info", "initialize with options internet=$internet_interface, intranet1=$intranetA_interface, create_nat1=$create_natA, intranet2=$intranetB_interface, create_nat2=$create_natB") unless $debug;

# set default rules: DROP everything in INPUT/FORWARD
comment("set default policy: drop input/forward, accept output");
filter("--policy", "INPUT", "DROP");
filter("--policy", "FORWARD", "DROP");
filter("--policy", "OUTPUT", "ACCEPT");

# set default rules: ACCEPT all on loopback
comment("set default policy: accept traffic on loopback ($loopback_interface)");
filter("--append", "INPUT", "--in-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "OUTPUT", "--out-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "FORWARD", "--in-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "FORWARD", "--out-interface", $loopback_interface, "--jump", "ACCEPT");

# set default rules: ACCEPT INPUT responses 
foreach my $interface ($internet_interface, $intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    comment("set default policy: accept input responses on $interface");
    filter("--append", "INPUT", 
	   "--in-interface", $interface,
	   "--match", "state",
	   "--state", "ESTABLISHED,RELATED",
	   "--jump", "ACCEPT");
    filter("--append", "OUTPUT", 
	   "--out-interface", $interface,
	   "--match", "state",
	   "--state", "NEW,ESTABLISHED,RELATED",
	   "--jump", "ACCEPT");	
}

# give unconditional access to priviledged IPs on internet interface
foreach my $ip (@internet_priviledged_ips) {
    comment("accept traffic from priviledged IP $ip through $internet_interface");
    filter("--append", "INPUT",
	   "--in-interface", $internet_interface,
	   "--source", $ip,
	   "--jump", "ACCEPT");
    filter("--append", "OUTPUT",
	   "--out-interface", $internet_interface,
	   "--destination", $ip,
	   "--jump", "ACCEPT");
}

# create the NAT
foreach my $interface ($intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    if ($create_natA and $interface eq $intranetA_interface or
	$create_natB and $interface eq $intranetB_interface) {
	comment("create the network address translation (NAT) $interface (intranet) <-> $internet_interface (internet)");
	filter("--append", "FORWARD",
	       "--in-interface", $internet_interface,
	       "--out-interface", $interface,
	       "--match", "state",
	       "--state", "ESTABLISHED,RELATED",
	       "--jump", "ACCEPT");
	filter("--append", "FORWARD",
	       "--in-interface", $interface,
	       "--out-interface", $internet_interface,
	       "--match", "state",
	       "--state", "NEW,ESTABLISHED,RELATED",
	       "--jump", "ACCEPT");
	filter("--append", "POSTROUTING",
	       "--table", "nat",
	       "--out-interface", $internet_interface,
	       "--jump", "MASQUERADE");
    } else {
	comment("here we would create the network address translation (NAT) for $interface if we were asked to - we're not");
    }
}

# now deal with each requested port - without overthinking it, if wildcard
# * appears, we wont skip rules apparently irrelevant since then
comment("deal specifically with internet ($internet_interface) input");
filter_list($internet_interface, @internet_ports);

# now deal with each requested port - without overthinking it, if wildcard
# * appears, we wont skip rules apparently irrelevant since then
if ($intranetA_interface ne "") {
    comment("deal specifically with intranet ($intranetA_interface) input");
    filter_list($intranetA_interface, @intranetA_ports);
}
if ($intranetB_interface ne "") {
    comment("deal specifically with intranet ($intranetB_interface) input");
    filter_list($intranetB_interface, @intranetB_ports);
}


 
syslog("info", "initialized.") unless $debug;
closelog();

# EOF
