#!/bin/bash
#
# Copyright (c) 2017 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

KITCHEN_USER=pi
KITCHEN_HOST=thishost
VNC_HOSTSFILE=$HOME/.vnc/hosts
PORT=`shuf -i 10000-20000 -n 1`

# source local config
if [ -e ~/.cuisinerc ]; then . ~/.cuisinerc; fi

# if missing hostsfiles, build it (assumes the directory exists at least)
if [ ! -e "$VNC_HOSTSFILE" ]; then
    echo "+$KITCHEN_HOST" > "$VNC_HOSTSFILE"
fi

case "$(pidof x0vncserver | wc -w)" in
    1)
	# already on, we shut it off
	echo "cuisine is already on, shut if off"

	# video client
	killall -9 x0vncserver
	
	# audio stream
	killall -9 vlc
	
	# audio playback, assuming ssh connectivity is possible
	ssh "$KITCHEN_USER@$KITCHEN_HOST" "killall -9 mpg123"
	;;
    *)
	# off, we start it
	echo "start cuisine streaming"
	
	# start video client
	x0vncserver -HostsFile="$VNC_HOSTSFILE" -SecurityTypes=None &

	# start mp3 audio stream (that even mpg123 can play)
	cvlc -vvv pulse://`pactl list | grep "Monitor Source" | cut --delimiter ":" -f 2 | tr -d [:blank:]` --sout "#transcode{acodec=mp3,ab=128,channels=2}:standard{access=http,dst=0.0.0.0:$PORT/pc.mp3}" &

	# start mp3 audio stream playback, assuming ssh connectivity is possible
	ssh "$KITCHEN_USER@$KITCHEN_HOST" "mpg123 http://$HOSTNAME:$PORT/pc.mp3" &	
	;;
esac
       
# EOF
