#!/bin/bash
for i in `blkid  | grep crypto_LUKS | cut -f 1 -d :`; do
    j=`basename $i`
    cryptsetup luksOpen $i $j
    mkdir /mnt/$j
    mount /dev/mapper/$j /mnt/$j
    if [ -e /mnt/$j/etc/debian_version ]; then
	echo "/mnt/$j = Debian"
	mount --bind /dev /mnt/$j/dev
	mount --bind /sys /mnt/$j/sys
	mount -t proc /proc /mnt/$j/proc
    fi
done
# EOF
