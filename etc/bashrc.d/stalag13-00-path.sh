# skip already sourced
[ "`declare -f Y00path-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y00path-sourced() {
    echo true;
}

# To promote usage of 'su -' vs simple su, with environment cleaned
# debian decided not to include /sbin in $PATH
# https://wiki.debian.org/NewInBuster
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918754
# 
# It is nice to raise awareness of the possible issues ("I've over the years
# seen countless bug reports against different
# components claiming $SOMETHING completely broke their system beyond all
# repair, while in fact the problem was that they where running a root
# shell with environment inherited from their main user account.")
# but it is still a nuisance and suggest terribly insane environment in main
# account.

if [[ ":$PATH:" != *":/usr/sbin:"* ]]; then
   export PATH=/sbin:/usr/sbin:$PATH
fi 

# EOF
