# skip already sourced
[ "`declare -f sshwithauthsock 2>/dev/null`" ] && return

function sshwithauthsock {
    if [ ! -S ~/.ssh/ssh_auth_sock ]; then
	eval `ssh-agent`
	ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
    fi
    export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
    ssh-add -l > /dev/null || ssh-add
    "$@" 
}

alias ssh='sshwithauthsock ssh'
alias scp='sshwithauthsock scp'


# EOF
