# skip already sourced
[ "`declare -f Y60-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y60-sourced() {
    echo true;
}

# skip if gs isnt installed
[ ! -x "`which gs`" ] && return


SCAN2PDF_DIRECTORY=~/tmprm/scan
# depends on your scanner speed
SCAN2PDF_DPI=300
# use paperconf to get sensible default
SCAN2PDF_PAPER=`paperconf`
# depends on what your scanner backend support
# (top left is useful for flatbed; to scan A5, top-left 36 should be fine)
SCAN2PDF_SCANIMAGE_OPTIONS="--brightness -20 --contrast 15"
SCAN2PDF_SCANIMAGE_OPTIONS_TOPLEFT="-l 0"
SCAN2PDF_SCANIMAGE_OPTIONS_BW="--mode Lineart"
SCAN2PDF_SCANIMAGE_OPTIONS_COLOR="--mode Color"
SCAN2PDF_SCANIMAGE_OPTIONS_MODE="$SCAN2PDF_SCANIMAGE_OPTIONS_COLOR"
# default options for gs to build PDF
# (select /printer quality for 300dpi, /ebook for less)
# use all available core (by setting if not set to a number already)
case $SCAN2PDF_CPU_COUNT in  ''|*[!0-9]*) SCAN2PDF_CPU_COUNT=`awk '/^processor/{n+=1}END{print n}' /proc/cpuinfo`;; esac
SCAN2PDF_GS_OPTIONS="-dNumRenderingThreads=$SCAN2PDF_CPU_COUNT -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer"
# default options for ocrmypdf
SCAN2PDF_OCRMYPDF_OPTIONS="--rotate-pages"
SCAN2PDF_OCRMYPDF_OPTIONS_LANGUAGE="--language eng+fra+pol"


# beep is broken in many stupid ways on ubuntu at the moment
# this function will beep with play/sox if beep is not available
function rebeep {
    # $1 frequency
    # $2 length
    BEEP=`which beep`
    PLAY=`which play`
    # need either beep or play (sox) installed
    [ ! -x "$BEEP" ] && [ ! -x "$PLAY" ] && return
    # by default use beep if available unless we are on ubuntu
    if [ `grep ^ID=ubuntu$ /etc/os-release` ] || [ ! -x "$BEEP" ]; then
	[ ! -x "$PLAY" ] && echo "unable to run play (sox)!" && return
	($PLAY /usr/share/sounds/KDE-Im-Message-In.ogg synth .$2 $1 >/dev/null 2>/dev/null &)
    else
	[ ! -x "$BEEP" ] && echo "unable to run beep!" && return
	($BEEP -f $1 -l $2 &) 
    fi

}

# merge multiples PDF into one
function pdfmerge {
    ENDFILE=$1
    [ "$ENDFILE" == "" ] && echo "filename: " && read ENDFILE
    # skip if endfile exists already
    [ -e "$ENDFILE".pdf ] && return

    # merge pdf
    echo -e "merging \033[1;34m$ENDFILE*\033[0m..."
    gs -q -dNumRenderingThreads=$SCAN2PDF_CPU_COUNT -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -sOutputFile="$ENDFILE".pdf -f "$ENDFILE"*.pdf

    # linearize if qpdf is available
    if [ -x "`which qpdf`" ]; then
	echo -e "linearize \033[1;34m$ENDFILE.pdf\033[0m..."
	TMPLINEAR=$(mktemp --suffix=.pdf)
	cp "$ENDFILE".pdf $TMPLINEAR
	trap 'rm -f -- "$TMPLINEAR"' INT TERM HUP EXIT
	qpdf --linearize $TMPLINEAR "$ENDFILE".pdf
    fi
    
    # beep when done
    rebeep 100 100
    # optional: remove files passed as arguments, if any
    LIST=${@:2}
    [ "$LIST" == "" ] && return    
    echo -e "Is \033[1;34m$ENDFILE\033[0m.pdf ok (delete source files)? [\033[1;32mY\033[0m/\033[1;31mn\033[0m]"
    read OK    
    [ "$OK" == "n" ] && return
    [ "$OK" == "N" ] && return
    rm -f $LIST
}

# scan multiple A4 pages and merge them 
function scan2pdf {
    
    # check for mandatory binaries
    for bin in scanimage paperconf tiff2ps gs ocrmypdf convert mogrify qpdf; do
	if [ ! -x "`which $bin`" ]; then
	    echo "$bin not found, please install it.";
	    return 1
	fi
    done

   
    # go in work directory
    cd $SCAN2PDF_DIRECTORY

    # set options
    BATCH_MODE=0
    RECTOVERSO_MODE=0
    ROTATE=0
    ROTATE_VERSO=0
    ORIENTATION="P"
    while (("$#")); do
	case x"$1" in
	    x-h|x--help)
		echo -e "  -c, --color\t\t color mode"
		echo -e "  -l, --lineart\t\t lineart/black-and-white mode"
		echo -e "  -bw          \t\t     \""
		echo -e "  -t n, --turn n \t counter-clockwise turn the pages 90°/270° (landscape) or 180°"
		echo -e "  -trv         \t\t special tag to turn each verso +180°"
		echo ""
		echo -e "  -b n, --batch n\t batch mode (with document feeder), n setting the page count"
		echo -e "  -rv, --recto-verso\t two batches, recto then verso, verso coming in reverse order"
		echo -e "               \t\t (if more than two batches: first pages will be first page of each"
	    	echo -e "               \t\t that will probably be changed in the future)"
		echo ""
		echo -e "      --language\t language to use, as ocr-tesseract --list-langs proposes, for example:"
		echo -e "              \t\t   eng+fra+pol"
		echo ""
		echo -e "  -p x, --paper x\t paper type ($SCAN2PDF_PAPER), as paperconf -a sugggests"
		echo "" 
		echo -e "  -d, --device\t\t device to use, as scanimage -L proposes, for example:"
		echo -e "              \t\t   genesys:libusb:002:002"
		echo -e "              \t\t   canon_dr:libusb:005:003"
		echo ""
		echo "parameters preset using variables:"
		echo "  SCAN2PDF_DIRECTORY=$SCAN2PDF_DIRECTORY"
		echo "  SCAN2PDF_DPI=$SCAN2PDF_DPI"
		echo "  SCAN2PDF_PAPER=$SCAN2PDF_PAPER"
		echo " "	
		echo "scanimage parameters/options preset using variables:"
		echo "  SCAN2PDF_SCANIMAGE_OPTIONS=\"$SCAN2PDF_SCANIMAGE_OPTIONS\""
		echo "  SCAN2PDF_SCANIMAGE_OPTIONS_TOPLEFT=\"$SCAN2PDF_SCANIMAGE_OPTIONS_TOPLEFT\""
		echo "  SCAN2PDF_SCANIMAGE_OPTIONS_BW=\"$SCAN2PDF_SCANIMAGE_OPTIONS_BW\""
		echo "  SCAN2PDF_SCANIMAGE_OPTIONS_COLOR=\"$SCAN2PDF_SCANIMAGE_OPTIONS_COLOR\""
		echo "  SCAN2PDF_SCANIMAGE_OPTIONS_MODE=\"\$SCAN2PDF_SCANIMAGE_OPTIONS_COLOR\""
		echo "  SCAN2PDF_SCANIMAGE_DEVICE=\"$SCAN2PDF_SCANIMAGE_DEVICE\""
		echo " "
		echo "ps parameters/options preset using variables:"
		echo "  SCAN2PDF_GS_OPTIONS=\"$SCAN2PDF_GS_OPTIONS\""
		echo " "
		echo "ocrmypdf parameters/options preset using variables:"
		echo "  SCAN2PDF_OCRMYPDF_OPTIONS=\"$SCAN2PDF_OCRMYPDF_OPTIONS\""
		echo "  SCAN2PDF_OCRMYPDF_OPTIONS_LANGUAGE=\"$SCAN2PDF_OCRMYPDF_OPTIONS_LANGUAGE\""
		
		return
		;;
	    x-c|x--color)
		SCAN2PDF_SCANIMAGE_OPTIONS_MODE="$SCAN2PDF_SCANIMAGE_OPTIONS_COLOR"
		;;
	    x-bw|x-l|x--lineart)
		SCAN2PDF_SCANIMAGE_OPTIONS_MODE="$SCAN2PDF_SCANIMAGE_OPTIONS_BW"
		;;
	    x-p|x--paper)
		SCAN2PDF_PAPER=$2
		shift 1
		;;
	    x-b|x--batch)
		# 1 is uninterrupted batch
		# (scanimage should, itself, use batch mode to scan infinity if the scanner has a feeder)
		BATCH_MODE=1
		# but it could be defined
		case x"$2" in
		    x|x*[!0-9]*)
			echo -e "\033[1;33mbatch\033[0m mode selected \033[1;33mwithout page count\033[0m${shell_clear}, CTRL-C might be necessary to interrupt."
			;;
		    *)
			BATCH_MODE=$2
			shift 1
			;;
		esac	    
		;;
	    x-rv|--recto-verso)
		if (($BATCH_MODE < 1)); then BATCH_MODE=1 ; fi
		RECTOVERSO_MODE=1
		;;
	    x--language)
		# no second guess about the given language, take next argument as valid
		SCAN2PDF_OCRMYPDF_OPTIONS_LANGUAGE="--language $2"
		shift 1
		;;
	    x-d|x--device)
		# no second guess about the given device, take next argument as valid
		SCAN2PDF_SCANIMAGE_DEVICE="--device-name=$2 "
		shift 1
		;;
	    x-t|x--turn)
		case x"$2" in
		    x270)
			ROTATE="$2"
			ORIENTATION="L"
			shift 1
			;;
		    x180)
			ROTATE="$2"
			ORIENTATION="P"
			shift 1
			;;		    
		    *)
			# no value is equal to 90
			ROTATE="90"
			ORIENTATION="L"
			;;
		esac
		;;
	    x-trv)
		ROTATE_VERSO=1		
		;;
	    *)
		ENDFILE="$1"
		;;
	esac
	shift
    done

    # defines paper size
    PAPER_HEIGHT=`paperconf $SCAN2PDF_PAPER -m -h | cut -f 1 -d " "`
    PAPER_WIDTH=`paperconf $SCAN2PDF_PAPER -m -w | cut -f 1 -d " "`

    # prompt user if no filename argument passed
    [ "$ENDFILE" == "" ] && echo "filename: " && read ENDFILE
    # remove any ending .pdf from the name
    ENDFILE="${ENDFILE%.pdf}" 
    ENDFILE="${ENDFILE%.PDF}"   
    # exists already? then treat it as first (000) page + ending random string
    [ -e "$ENDFILE".pdf ] && mv "$ENDFILE".pdf "$ENDFILE"`mktemp --dry-run 000eXXX`.pdf && echo -e "\033[1;34m$ENDFILE\033[0m is now \033[1;34m$ENDFILE""000\033[0m"

    # scan one page/batch at a time
    VERSO_MAX=0
    RECTO_MAX=0
    for i in `seq -f %04g 999`; do
	if (($RECTOVERSO_MODE)); then
	    if (($i % 2)); then
		echo -e "scanning \033[1;34mrecto\033[0m"
	    else
		echo -e "scanning \033[1;34mverso\033[0m"
	    fi
	fi

	# avoid overwriting already existing PDF
	[ -e "$ENDFILE"-0001-$i.pdf ] && echo "exists already" && continue
	[ -e "$ENDFILE"-$i-0001.pdf ] && echo "exists already" && continue
	[ -e "$ENDFILE"-$i.pdf ] && echo "exists already" && continue
	
	# scan with decent contrast for text
	# with necessary options to scan the whole page including margins
	echo -e ">tiff \c"
	CMD="scanimage --format=tiff $SCAN2PDF_SCANIMAGE_OPTIONS_TOPLEFT --page-width $PAPER_WIDTH -x $PAPER_WIDTH --page-height $PAPER_HEIGHT -y $PAPER_HEIGHT $SCAN2PDF_SCANIMAGE_DEVICE $SCAN2PDF_SCANIMAGE_OPTIONS $SCAN2PDF_SCANIMAGE_OPTIONS_MODE --resolution=$SCAN2PDF_DPI"
	if (($BATCH_MODE)); then
	    # if > 1, set to batch mode with counter
	    if (($BATCH_MODE > 1)); then CMD="$CMD --batch-count=$BATCH_MODE" ; fi
	    # in recto-verso mode
	    # (scanimage allows to play with increment to handle recto verso, but it means that you actually have
	    # to reorder files, the first to be scanned being actually the last verso)
	    if (($RECTOVERSO_MODE)); then
		# in recto-verso mode, the filename is altered (page1-batch1 page1-batch2 page2-batch1 page2-batch2)
		$CMD --batch="$ENDFILE"-%04d-$i.tiff

		if (($i == 1)); then 
		    # record the number of pages of the first recto
		    RECTO_MAX=`ls -Uba1 | grep ^\$ENDFILE-[0-9]*-\$i.tiff$ | wc -l`
		elif (($i % 2)); then
		    # uneven but no one mean we are dealing with another recto, stop scanning here
		    # (todo: add possibility to continue independantly from the first two recto/verso batches)
		    echo 
		else
		    # even is verso, reverse the file order
		    VERSO_MAX=`ls -Uba1 | grep ^\$ENDFILE-[0-9]*-\$i.tiff$ | wc -l`

		    # loop until we have the same number of pages as recto
		    # (useful when having issues with feeder and thin paper)
		    while (($VERSO_MAX != $RECTO_MAX)); do
			echo -e "\033[1;33mpage count mismatch\033[0m, $RECTO_MAX recto vs $VERSO_MAX verso."
			echo -e "redo last batch? [\033[1;32mY\033[0m/\033[1;31mn\033[0m]"
			read REDO
			[ "${REDO,,}" == "n" ] && break
			rm -f "$ENDFILE"-*-$i.tiff
			$CMD --batch="$ENDFILE"-%04d-$i.tiff
			VERSO_MAX=`ls -Uba1 | grep ^\$ENDFILE-[0-9]*-\$i.tiff$ | wc -l`
		    done		    
		    
		    TMPVERSO=$(mktemp --directory)
		    trap 'rm -fr -- "$TMPVERSO"' INT TERM HUP EXIT
		    # in two steps not to overwrite anything
		    for j in `seq -f %04g $VERSO_MAX`; do
			mv "$ENDFILE"-$j-$i.tiff $TMPVERSO/`printf "%.4d" $(($VERSO_MAX + 1 - 10#$j))`
		    done
		    for j in `seq -f %04g $VERSO_MAX`; do
			mv $TMPVERSO/$j "$ENDFILE"-$j-$i.tiff
			# we might want an extra 180 rotation
			if (($ROTATE_VERSO)); then
			    mogrify -rotate "-180" "$ENDFILE"-$j-$i.tiff
			fi
		    done
		fi
		
	    else
		# otherwise run the batch command with default naming (batch1-page1 batch1-page2 batch2-page1...)
		$CMD --batch="$ENDFILE"-$i-%04d.tiff
	    fi
	else
	    # single file
	    $CMD > "$ENDFILE"-$i.tiff
	fi
	
	# beep when scanning is done (and so when the page can be changed with a flatbed)
	rebeep 100 025
	
	# beep when prompting user
	rebeep 100 025
	rebeep 01 025
	rebeep 100 025
	
	# by default scan another page/batch
	if (($RECTOVERSO_MODE)); then
	    if (($i % 2)); then
		# uneven mean we did recto and are about to do verso
		echo -e "verso ready? [\033[1;32mY\033[0m/\033[1;31mn\033[0m]"
	    else
		# otherwise, we just did verso, stop scanning here
		# (todo: add possibility to continue independantly from the first two recto/verso batches)
		break
	    fi	    
	else
	    echo -e "another page/batch? [\033[1;32mY\033[0m/\033[1;31mn\033[0m]"
	fi
	read NEXT
	[ "${NEXT,,}" == "n" ] && break
    done
    
    # make a PDF out of every TIFF
    for TIFF in "$ENDFILE"*.tiff; do	   	   
	TMPPS=$(mktemp --suffix=.ps)
	trap 'rm -f -- "$TMPPS"' INT TERM HUP EXIT

	echo -e "$TIFF >ps \c"
	
	# convert to postscript (level 3, among other things to avoid "Inconsistent value of es" issue)
	tiff2ps -3 -r $ROTATE -P $ORIENTATION "$TIFF" > $TMPPS

	# create PDF, if not blank page, test that will be efficient only in lineart mode
	# (https://superuser.com/questions/1248528/remove-blank-pages-from-pdf-from-command-line)
	INKCOVERAGE=`gs -o - -sDEVICE=inkcov $TMPPS | grep CMYK | nawk 'BEGIN { sum=0; } {sum += $1 + $2 + $3 + $4;} END { printf "%.5f\n", sum } '`
	if (( $(awk 'BEGIN {print ("'$INKCOVERAGE'" > "'0.001'")}') )); then
	    echo -e ">pdf \c"
	    # set orientation to be kept as it is
	    gs -q -sPAPERSIZE=$SCAN2PDF_PAPER -sDEVICE=pdfwrite -sOutputFile="`basename "$TIFF" .tiff`".pdf -dNOPAUSE  -dBATCH -dEPSCrop -c "<</Orientation 0>> setpagedevice" -f $TMPPS
	else
	    echo -e ">skip blank page \c"
	fi

	rm -f "$TIFF" $TMPPS
	echo -e ""
    done
    
    # merge into one PDF
    pdfmerge "$ENDFILE" "$ENDFILE"*.pdf
    
    # add text recognition and rotate page
    # (as copy so if ocrmypdf fails to produce anything or is interrupted by user, we keep the original in
    # place)
    TMPOCR=$(mktemp --suffix=.pdf)
    cp "$ENDFILE".pdf $TMPOCR
    trap 'rm -f -- "$TMPOCR"' INT TERM HUP EXIT
    echo -e ">ocr \c"
    ocrmypdf $SCAN2PDF_OCRMYPDF_OPTIONS $SCAN2PDF_OCRMYPDF_OPTIONS_LANGUAGE $TMPOCR "$ENDFILE".pdf

    # linearize
    TMPLINEAR=$(mktemp --suffix=.pdf)
    cp "$ENDFILE".pdf $TMPLINEAR
    trap 'rm -f -- "$TMPLINEAR"' INT TERM HUP EXIT
    echo -e ">linearize \c"
    qpdf --linearize $TMPLINEAR "$ENDFILE".pdf
    
}


# EOF
