# skip already sourced
[ "`declare -f Y70-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y70-sourced() {
    echo true;
}

# whatever from 8 to 16 chars
alias pw='< /dev/urandom tr -d "/\",;\`/d" | tr -dc [:graph:] | head -c`shuf -i8-16 -n1` && echo'

# alphanumeric only 8 to 16 chars
alias pwalt='< /dev/urandom tr -dc [:alnum:] | head -c`shuf -i8-16 -n1` && echo'

# EOF
