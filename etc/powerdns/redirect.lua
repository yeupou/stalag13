-- IPv4 only script
-- Copyright (c) 2017 Mathieu Roy <yeupou--gnu.org>
--                 http://yeupou.wordpress.com
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
-- USA

-- cached servers
cached = newDS()
cachedest = "10.0.0.88"

-- ads kill list
ads = newDS()
adsdest = "127.0.0.1"

-- hand maintained black list
blacklisted = newDS()
blacklistdest = "127.0.0.1"

function preresolve(dq)
   -- DEBUG
   --pdnslog("Got question for "..dq.qname:toString().." from "..dq.remoteaddr:toString().." to "..dq.localaddr:toString(), pdns.loglevels.Error)
   
   -- handmade domains blacklist
   if(blacklisted:check(dq.qname)) then
      if(dq.qtype == pdns.A) then
	 dq:addAnswer(dq.qtype, blacklistdest)
	 return true
      end
   end
   
   -- spam/ads domains
   if(ads:check(dq.qname)) then
      if(dq.qtype == pdns.A) then
	 dq:addAnswer(dq.qtype, adsdest)
	 return true
      end
   end						
   
   -- cached domains
   if(not cached:check(dq.qname)) then
      -- not cached
      return false
   else
      -- cached: variable answer
      dq.variable = true
      -- request coming from the cache itself
      if(dq.remoteaddr:equal(newCA(cachedest))) then
	 return false
      end
      --  redirect to the cache
      if(dq.qtype == pdns.A) then
	 dq:addAnswer(dq.qtype, cachedest)
      end
   end
   
   return true
end

cached:add(dofile("/etc/powerdns/redirect-cached.lua"))
ads:add(dofile("/etc/powerdns/redirect-ads.lua"))
blacklisted:add(dofile("/etc/powerdns/redirect-blacklisted.lua"))

-- EOF
