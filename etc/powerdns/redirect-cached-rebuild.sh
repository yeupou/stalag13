#!/bin/sh

DOMAINS=""

# comment this if you dont cache steam
# (note: nginx cache must also cover this)
DOMAINS="$DOMAINS cs.steampowered.com content1.steampowered.com content2.steampowered.com content3.steampowered.com content4.steampowered.com content5.steampowered.com content6.steampowered.com content7.steampowered.com content8.steampowered.com content9.steampowered.com hsar.steampowered.com.edgesuite.net akamai.steamstatic.com content-origin.steampowered.com client-download.steampowered.com steampipe.steamcontent.com steamcontent.com"
# comment this if you dont cache debian
# (useful read: https://wiki.debian.org/DebianGeoMirror )
DOMAINS="$DOMAINS cdn-fastly.deb.debian.org ftp.fr.debian.org ftp.de.debian.org ftp.debian.org security.debian.org"
# comment this if you dont cache devuan
DOMAINS="$DOMAINS packages.devuan.org amprolla.devuan.org"
# comment this if you dont cache ubuntu
DOMAINS="$DOMAINS fr.archive.ubuntu.com security.ubuntu.com"

out=redirect-cached.lua
echo "-- build by ${0}" > $out
echo "-- re-run it commenting relevant domains if you dont cache them all" >> $out
echo "return{" >> $out
for domain in $DOMAINS; do
    echo \"$domain\", >> $out
done
echo "}" >> $out
    
# EOF
