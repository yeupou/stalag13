# If running bash, make sure we always run /etc/bashrc.d/
[ -z "$BASH_VERSION" ] && return

for i in /etc/bashrc.d/*.sh ; do if [ -r "$i" ]; then . $i; fi; done

# EOF

